<?php
/**
 * Page to handle all Ajax requests for add data.
 * All data will be returned using JSON format
 *
 * General variables:
 *
 * @uses	$_REQUEST['object']			required
 */

/** Loads the WordPress Environment */
require_once ( dirname(__FILE__) . '/../../../../../wp-load.php' );

// Adding
if ( isset($_REQUEST['action']) && 'add' == $_REQUEST['action'] && isset( $_REQUEST['submit_add_content'] ) ) {
	if (  isset($_REQUEST['name']) && '' != SuperContentInserter_Validator::parse_string( $_REQUEST['name']) ){
		check_admin_referer('SuperContentInserter-add-update-content');

		$data_to_add['name'] =				isset($_REQUEST['name']) ? SuperContentInserter_Validator::parse_string($_REQUEST['name']) : "";
		$data_to_add['content'] =			isset($_REQUEST['content_text']) ? $_REQUEST['content_text'] : "";
		$data_to_add['content_is_php'] =	isset($_REQUEST['content_is_php']) ? $_REQUEST['content_is_php'] : 0;
		$data_to_add['top_right_width'] =	isset($_REQUEST['top_right_width']) ? $_REQUEST['top_right_width'] : 10;
		$data_to_add['top_left_width'] =	isset($_REQUEST['top_left_width']) ? $_REQUEST['top_left_width'] : 10;
		// Get all checked position.
		$position = array();
		isset( $_REQUEST['position_init'] ) ? 					$position[] = 'init' : "";
		isset( $_REQUEST['position_header'] ) ?					$position[] = 'header' : "";
		isset( $_REQUEST['position_after_post_entry'] ) ? 		$position[] = 'after_post_entry' : "";
		isset( $_REQUEST['position_before_post_entry'] ) ? 		$position[] = 'before_post_entry' : "";
		isset( $_REQUEST['position_post_entry_top_right'] ) ? 	$position[] = 'post_entry_top_right' : "";
		isset( $_REQUEST['position_post_entry_top_left'] ) ? 	$position[] = 'post_entry_top_left' : "";
		isset( $_REQUEST['position_top_posts_sequence'] ) ? 		$position[] = 'top_posts_sequence' : "";
		isset( $_REQUEST['position_bottom_posts_sequence'] ) ? 	$position[] = 'bottom_posts_sequence' : "";
		isset( $_REQUEST['position_footer'] ) ? 					$position[] = 'footer' : "";

		// Convert position intro sting, separate by ";" character
		$data_to_add['position'] = implode(';',$position);

		// Store show in intro db
		$shown_in = array();
		if ( !isset( $_REQUEST['show_in_all'] ) ){
			// Selected home?
			if ( isset( $_REQUEST['show_in_home'] ) )
				$shown_in[] = 'home';

			// Get Selected pages?
			if ( isset( $_REQUEST['show_in_specific_pages'] ) && !isset( $_REQUEST['show_in_specific_pages_selected'] ) ){
				$shown_in[] = 'pages::' . 'all_pages';
			} elseif ( isset( $_REQUEST['show_in_specific_pages'] ) && isset( $_REQUEST['show_in_specific_pages_selected'] ) ) {
				$shown_in[] = 'pages::' . implode('::', $_REQUEST['show_in_specific_pages_selected']);
			}

			// Get selected posts?
			if ( isset( $_REQUEST['show_in_specific_posts'] ) && !isset( $_REQUEST['show_in_specific_posts_selected'] ) ){
				$shown_in[] = 'posts::' . 'all_posts';
			} elseif ( isset( $_REQUEST['show_in_specific_posts'] ) && isset( $_REQUEST['show_in_specific_posts_selected'] ) ) {
				$shown_in[] = 'posts::' . implode('::', $_REQUEST['show_in_specific_posts_selected']);
			}

			// Get selected categories?
			if ( isset( $_REQUEST['show_in_specific_categories'] ) && !isset( $_REQUEST['show_in_specific_categories_selected'] ) ){
				$shown_in[] = 'categories::' . 'all_categories';
			} elseif ( isset( $_REQUEST['show_in_specific_categories'] ) && isset( $_REQUEST['show_in_specific_categories_selected'] ) ) {
				$shown_in[] = 'categories::' . implode('::', $_REQUEST['show_in_specific_categories_selected'] );
			}

			// Get selected tags?
			if ( isset( $_REQUEST['show_in_specific_tags'] ) && !isset( $_REQUEST['show_in_specific_tags_selected'] ) ){
				$shown_in[] = 'tags::' . 'all_tags';
			} elseif ( isset( $_REQUEST['show_in_specific_tags'] ) && isset( $_REQUEST['show_in_specific_tags_selected'] ) ) {
				$shown_in[] = 'tags::' . implode('::', $_REQUEST['show_in_specific_tags_selected'] );
			}

			// Selected archives?
			if ( isset( $_REQUEST['show_in_archives'] ) )
				$shown_in[] = 'archives';

			// Selected category page?
			if ( isset( $_REQUEST['show_in_category_page'] ) && !isset( $_REQUEST['show_in_specific_categories_page_selected'] ) ){
				$shown_in[] = 'category_page::' . 'all_category_page';
			} elseif ( isset( $_REQUEST['show_in_category_page'] ) && isset( $_REQUEST['show_in_specific_categories_page_selected'] ) ) {
				$shown_in[] = 'category_page::' . implode('::', $_REQUEST['show_in_specific_categories_page_selected'] );
			}

			// Selected tag page?
			if ( isset( $_REQUEST['show_in_tags_page'] ) && !isset( $_REQUEST['show_in_specific_tags_page_selected'] ) ){
				$shown_in[] = 'tags_page::' . 'all_tags_page';
			} elseif ( isset( $_REQUEST['show_in_tags_page'] ) && isset( $_REQUEST['show_in_specific_tags_page_selected'] ) ) {
				$shown_in[] = 'tags_page::' . implode('::', $_REQUEST['show_in_specific_tags_page_selected'] );
			}

			// Selected 404 page?
			if ( isset( $_REQUEST['show_in_404_page'] ) )
				$shown_in[] = '404_page';

		} else {
			// Select all by default
			$shown_in[] = 'all';
		}

		$data_to_add['shown_in'] = implode(';',$shown_in);

		// Store show in intro db
		$not_shown_in = array();
		if ( !isset( $_REQUEST['not_show_in_all'] ) ){
			// Selected home?
			if ( isset( $_REQUEST['not_show_in_home'] ) )
				$not_shown_in[] = 'home';

			// Get Selected pages?
			if ( isset( $_REQUEST['not_show_in_specific_pages'] ) && !isset( $_REQUEST['not_show_in_specific_pages_selected'] ) ){
				$not_shown_in[] = 'pages::' . 'all_pages';
			} elseif ( isset( $_REQUEST['not_show_in_specific_pages'] ) && isset( $_REQUEST['not_show_in_specific_pages_selected'] ) ) {
				$not_shown_in[] = 'pages::' . implode('::', $_REQUEST['not_show_in_specific_pages_selected']);
			}

			// Get selected posts?
			if ( isset( $_REQUEST['not_show_in_specific_posts'] ) && !isset( $_REQUEST['not_show_in_specific_posts_selected'] ) ){
				$not_shown_in[] = 'posts::' . 'all_posts';
			} elseif ( isset( $_REQUEST['not_show_in_specific_posts'] ) && isset( $_REQUEST['not_show_in_specific_posts_selected'] ) ) {
				$not_shown_in[] = 'posts::' . implode('::', $_REQUEST['not_show_in_specific_posts_selected']);
			}

			// Get selected categories?
			if ( isset( $_REQUEST['not_show_in_specific_categories'] ) && !isset( $_REQUEST['not_show_in_specific_categories_selected'] ) ){
				$not_shown_in[] = 'categories::' . 'all_categories';
			} elseif ( isset( $_REQUEST['not_show_in_specific_categories'] ) && isset( $_REQUEST['not_show_in_specific_categories_selected'] ) ) {
				$not_shown_in[] = 'categories::' . implode('::', $_REQUEST['not_show_in_specific_categories_selected'] );
			}

			// Get selected tags?
			if ( isset( $_REQUEST['not_show_in_specific_tags'] ) && !isset( $_REQUEST['not_show_in_specific_tags_selected'] ) ){
				$not_shown_in[] = 'tags::' . 'all_tags';
			} elseif ( isset( $_REQUEST['not_show_in_specific_tags'] ) && isset( $_REQUEST['not_show_in_specific_tags_selected'] ) ) {
				$not_shown_in[] = 'tags::' . implode('::', $_REQUEST['not_show_in_specific_tags_selected'] );
			}

			// Selected archives?
			if ( isset( $_REQUEST['not_show_in_archives'] ) )
				$not_shown_in[] = 'archives';

			// Selected category page?
			if ( isset( $_REQUEST['not_show_in_category_page'] ) && !isset( $_REQUEST['not_show_in_specific_categories_page_selected'] ) ){
				$not_shown_in[] = 'category_page::' . 'all_category_page';
			} elseif ( isset( $_REQUEST['not_show_in_category_page'] ) && isset( $_REQUEST['not_show_in_specific_categories_page_selected'] ) ) {
				$not_shown_in[] = 'category_page::' . implode('::', $_REQUEST['not_show_in_specific_categories_page_selected'] );
			}

			// Selected tag page?
			if ( isset( $_REQUEST['not_show_in_tags_page'] ) && !isset( $_REQUEST['not_show_in_specific_tags_page_selected'] ) ){
				$not_shown_in[] = 'tags_page::' . 'all_tags_page';
			} elseif ( isset( $_REQUEST['not_show_in_tags_page'] ) && isset( $_REQUEST['not_show_in_specific_tags_page_selected'] ) ) {
				$not_shown_in[] = 'tags_page::' . implode('::', $_REQUEST['not_show_in_specific_tags_page_selected'] );
			}

			// Selected 404 page?
			if ( isset( $_REQUEST['not_show_in_404_page'] ) )
				$not_shown_in[] = '404_page';

		} else {
			// Select all
			$not_shown_in[] = 'all';
		}

		$data_to_add['not_shown_in'] = implode(';',$not_shown_in);

		if ( !SuperContentInserter_Content::already_by_name($data_to_add['name']) ) {
			if ( $content_added_id = SuperContentInserter_Content::add($data_to_add) ) {
				$data_to_add['shortcode'] = '[sci id=' . $content_added_id . ']';
				$data['id'] = $content_added_id;
				SuperContentInserter_Content::update($data_to_add, array( 'id' => $content_added_id ));
				$message_to_return = __('The content block was added successfully. The shortcode is ' . $data_to_add['shortcode'] . '.' ,'super-content-insert');
				$type_to_return = 'notification';
				$redirect_url = admin_url('admin.php?page=super-content-inserter-add-edit.php&is_editing=1&id=' . $content_added_id . "&added=" . $type_to_return);
			} else {
				$message_to_return = __('Error inserting into database.','super-content-insert');
				$type_to_return = 'error';
			}
		} else {
			$message_to_return = __('The name already exists.','super-content-insert');
			$type_to_return = 'error';
		}
	} else {
		$message_to_return = __('The content block name is required.','super-content-insert');
		$type_to_return = 'error';
	}
}
// Updating
if ( isset($_REQUEST['action']) && 'add' == $_REQUEST['action'] && isset( $_REQUEST['submit_update_content'] ) &&
	isset($_REQUEST['content_id']) && '' != SuperContentInserter_Validator::parse_int($_REQUEST['content_id'])) {
	if ( isset($_REQUEST['name']) && '' != SuperContentInserter_Validator::parse_string( $_REQUEST['name']) ) {
		check_admin_referer('SuperContentInserter-add-update-content');

		$data_old = SuperContentInserter_Content::get(SuperContentInserter_Validator::parse_int($_REQUEST['content_id']) );

		$data_to_modificate = array();

		if ( isset( $data_old ) ){

			$data_to_modificate['id'] = SuperContentInserter_Validator::parse_int($_REQUEST['content_id']);

			$data_to_update['name'] =			isset( $_REQUEST['name'] ) 			? SuperContentInserter_Validator::parse_string($_REQUEST['name']) : "";
			$data_to_update['content'] =		isset( $_REQUEST['content_text'] ) 	? $_REQUEST['content_text'] : "";
			$data_to_update['content_is_php'] =	isset( $_REQUEST['content_is_php'] )? $_REQUEST['content_is_php'] : 0 ;
			$data_to_update['top_right_width'] =isset( $_REQUEST['top_right_width'] )? $_REQUEST['top_right_width'] : 10;
			$data_to_update['top_left_width'] =	isset( $_REQUEST['top_left_width'] )? $_REQUEST['top_left_width'] : 10;
			// Get all checked position.
			$position = array();
			isset( $_REQUEST['position_init'] ) ? 					$position[] = 'init' : "";
			isset( $_REQUEST['position_header'] ) ? 				$position[] = 'header' : "";
			isset( $_REQUEST['position_after_post_entry'] ) ? 		$position[] = 'after_post_entry' : "";
			isset( $_REQUEST['position_before_post_entry'] ) ? 		$position[] = 'before_post_entry' : "";
			isset( $_REQUEST['position_post_entry_top_right'] ) ? 	$position[] = 'post_entry_top_right' : "";
			isset( $_REQUEST['position_post_entry_top_left'] ) ? 	$position[] = 'post_entry_top_left' : "";
			isset( $_REQUEST['position_top_posts_sequence'] ) ? 		$position[] = 'top_posts_sequence' : "";
			isset( $_REQUEST['position_bottom_posts_sequence'] ) ? 	$position[] = 'bottom_posts_sequence' : "";
			isset( $_REQUEST['position_footer'] ) ? 					$position[] = 'footer' : "";

			// Convert position intro sting, separate by ";" character
			$data_to_update['position'] = implode(';',$position);

			// Store show in intro db
			$shown_in = array();
			if ( !isset($_REQUEST['show_in_all']) ){
				// Selected home?
				if ( isset( $_REQUEST['show_in_home'] ) )
					$shown_in[] = 'home';

				// Get Selected pages?
				if ( isset( $_REQUEST['show_in_specific_pages'] ) && !isset( $_REQUEST['show_in_specific_pages_selected'] ) ){
					$shown_in[] = 'pages::' . 'all_pages';
				} elseif ( isset( $_REQUEST['show_in_specific_pages'] ) && isset( $_REQUEST['show_in_specific_pages_selected'] ) ) {
					$shown_in[] = 'pages::' . implode('::', $_REQUEST['show_in_specific_pages_selected']);
				}

				// Get selected posts?
				if ( isset( $_REQUEST['show_in_specific_posts'] ) && !isset( $_REQUEST['show_in_specific_posts_selected'] ) ){
					$shown_in[] = 'posts::' . 'all_posts';
				} elseif ( isset( $_REQUEST['show_in_specific_posts'] ) && isset( $_REQUEST['show_in_specific_posts_selected'] ) ) {
					$shown_in[] = 'posts::' . implode('::', $_REQUEST['show_in_specific_posts_selected']);
				}

				// Get selected categories?
				if ( isset( $_REQUEST['show_in_specific_categories'] ) && !isset( $_REQUEST['show_in_specific_categories_selected'] ) ){
					$shown_in[] = 'categories::' . 'all_categories';
				} elseif ( isset( $_REQUEST['show_in_specific_categories'] ) && isset( $_REQUEST['show_in_specific_categories_selected'] ) ) {
					$shown_in[] = 'categories::' . implode('::', $_REQUEST['show_in_specific_categories_selected'] );
				}

				// Get selected tags?
				if ( isset( $_REQUEST['show_in_specific_tags'] ) && !isset( $_REQUEST['show_in_specific_tags_selected'] ) ){
					$shown_in[] = 'tags::' . 'all_tags';
				} elseif ( isset( $_REQUEST['show_in_specific_tags'] ) && isset( $_REQUEST['show_in_specific_tags_selected'] ) ) {
					$shown_in[] = 'tags::' . implode('::', $_REQUEST['show_in_specific_tags_selected'] );
				}

				// Selected archives?
				if ( isset( $_REQUEST['show_in_archives'] ) )
					$shown_in[] = 'archives';

				// Selected category page?
				if ( isset( $_REQUEST['show_in_category_page'] ) && !isset( $_REQUEST['show_in_specific_categories_page_selected'] ) ){
					$shown_in[] = 'category_page::' . 'all_category_page';
				} elseif ( isset( $_REQUEST['show_in_category_page'] ) && isset( $_REQUEST['show_in_specific_categories_page_selected'] ) ) {
					$shown_in[] = 'category_page::' . implode('::', $_REQUEST['show_in_specific_categories_page_selected'] );
				}

				// Selected tag page?
				if ( isset( $_REQUEST['show_in_tags_page'] ) && !isset( $_REQUEST['show_in_specific_tags_page_selected'] ) ){
					$shown_in[] = 'tags_page::' . 'all_tags_page';
				} elseif ( isset( $_REQUEST['show_in_tags_page'] ) && isset( $_REQUEST['show_in_specific_tags_page_selected'] ) ) {
					$shown_in[] = 'tags_page::' . implode('::', $_REQUEST['show_in_specific_tags_page_selected'] );
				}

				// Selected 404 page?
				if ( isset( $_REQUEST['show_in_404_page'] ) )
					$shown_in[] = '404_page';

			} else {
				// Select all by default
				$shown_in[] = 'all';
			}

			$data_to_update['shown_in'] = implode(';',$shown_in);

			// Store show in intro db
			$not_shown_in = array();
			if ( !isset( $_REQUEST['not_show_in_all'] ) ){
				// Selected home?
				if ( isset( $_REQUEST['not_show_in_home'] ) )
					$not_shown_in[] = 'home';

				// Get Selected pages?
				if ( isset( $_REQUEST['not_show_in_specific_pages'] ) && !isset( $_REQUEST['not_show_in_specific_pages_selected'] ) ){
					$not_shown_in[] = 'pages::' . 'all_pages';
				} elseif ( isset( $_REQUEST['not_show_in_specific_pages'] ) && isset( $_REQUEST['not_show_in_specific_pages_selected'] ) ) {
					$not_shown_in[] = 'pages::' . implode('::', $_REQUEST['not_show_in_specific_pages_selected']);
				}

				// Get selected posts?
				if ( isset( $_REQUEST['not_show_in_specific_posts'] ) && !isset( $_REQUEST['not_show_in_specific_posts_selected'] ) ){
					$not_shown_in[] = 'posts::' . 'all_posts';
				} elseif ( isset( $_REQUEST['not_show_in_specific_posts'] ) && isset( $_REQUEST['not_show_in_specific_posts_selected'] ) ) {
					$not_shown_in[] = 'posts::' . implode('::', $_REQUEST['not_show_in_specific_posts_selected']);
				}

				// Get selected categories?
				if ( isset( $_REQUEST['not_show_in_specific_categories'] ) && !isset( $_REQUEST['not_show_in_specific_categories_selected'] ) ){
					$not_shown_in[] = 'categories::' . 'all_categories';
				} elseif ( isset( $_REQUEST['not_show_in_specific_categories'] ) && isset( $_REQUEST['not_show_in_specific_categories_selected'] ) ) {
					$not_shown_in[] = 'categories::' . implode('::', $_REQUEST['not_show_in_specific_categories_selected'] );
				}

				// Get selected tags?
				if ( isset( $_REQUEST['not_show_in_specific_tags'] ) && !isset( $_REQUEST['not_show_in_specific_tags_selected'] ) ){
					$not_shown_in[] = 'tags::' . 'all_tags';
				} elseif ( isset( $_REQUEST['not_show_in_specific_tags'] ) && isset( $_REQUEST['not_show_in_specific_tags_selected'] ) ) {
					$not_shown_in[] = 'tags::' . implode('::', $_REQUEST['not_show_in_specific_tags_selected'] );
				}

				// Selected archives?
				if ( isset( $_REQUEST['not_show_in_archives'] ) )
					$not_shown_in[] = 'archives';

				// Selected category page?
				if ( isset( $_REQUEST['not_show_in_category_page'] ) && !isset( $_REQUEST['not_show_in_specific_categories_page_selected'] ) ){
					$not_shown_in[] = 'category_page::' . 'all_category_page';
				} elseif ( isset( $_REQUEST['not_show_in_category_page'] ) && isset( $_REQUEST['not_show_in_specific_categories_page_selected'] ) ) {
					$not_shown_in[] = 'category_page::' . implode('::', $_REQUEST['not_show_in_specific_categories_page_selected'] );
				}

				// Selected tag page?
				if ( isset( $_REQUEST['not_show_in_tags_page'] ) && !isset( $_REQUEST['not_show_in_specific_tags_page_selected'] ) ){
					$not_shown_in[] = 'tags_page::' . 'all_tags_page';
				} elseif ( isset( $_REQUEST['not_show_in_tags_page'] ) && isset( $_REQUEST['not_show_in_specific_tags_page_selected'] ) ) {
					$not_shown_in[] = 'tags_page::' . implode('::', $_REQUEST['not_show_in_specific_tags_page_selected'] );
				}

				// Selected 404 page?
				if ( isset( $_REQUEST['not_show_in_404_page'] ) )
					$not_shown_in[] = '404_page';

			} else {
				// Select all
				$not_shown_in[] = 'all';
			}

			$data_to_update['not_shown_in'] = implode(';',$not_shown_in);

			if ( SuperContentInserter_Content::update( $data_to_update , $data_to_modificate ) ) {
				$message_to_return = __('The content block was updated successfully.','super-content-insert');
				$type_to_return = 'notification';
			} else {
				$message_to_return = __('Error inserting into database.','super-content-insert');
				$type_to_return = 'error';
			}
		}
	} else {
		$message_to_return = __('The content block name is required.','super-content-insert');
		$type_to_return = 'error';
	}
}

$data['message']	= $message_to_return;
$data['type'] 		= $type_to_return;
$data['data']['redirect_url'] = isset($redirect_url) ? $redirect_url : Null ;

$json = json_encode($data);
echo $json;
die();