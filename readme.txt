=== Super Content Inserter ===
Contributors: John Daniel
Tags: content,insert
Tested up to: 3.5
Requires at least: 3.5
Stable tag: trunk

== Description ==

Add and display Content into blog content in specific sections.

== Functionalities ==

Add custom content blocks into several areas of blog Posts or Pages.
These content blocks can be from pure text, HTML, banners or PHP code.

== Screenshots ==

1. The "Content Blocks List" page.
2. The "Add/Edit Content" page.

== Installation ==

1. Upload the whole plugin folder to your /wp-content/plugins/ folder.
2. Go to the 'Plugins' page in the menu and activate the plugin.

== How PHP content works ==

You can insert any valid PHP code in one content block by entering the code in the "Content" field and checking then
the field labeled by "Check if the content is PHP code." right below the content box.


An example of valid PHP code you can enter is like follows:

	echo "This is a text here";

	echo "<p>This is another text line</p>";

	echo "The title of this blog is " . get_bloginfo('title');

Also you can mix PHP around HTML content but then you must add at the beginning the PHP ending tag "?>",
and enclose every PHP code inside <?php (...) ?> tags.

Example 1: NOTE: Bad code ahead! This example won't work:

	<ul>
		<li>
			echo "This is a list item.";
		</li>
		<li>
			echo get_bloginfo('title');
		</li>
	</ul>

Example 2: But this will work:

	?>

	<ul>
		<li>
			<?php echo "This is a list item.";?>
		</li>
		<li>
			<?php echo get_bloginfo('title');?>
		</li>
	</ul>