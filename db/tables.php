<?php
global $wpdb;

$prefix = $wpdb->prefix . SuperContentInserter::$db_prefix;

/*
 * Creating Table for Content
 */
$table_name = $prefix . SuperContentInserter_Content::$table_name;
if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
	$wpdb->query("CREATE TABLE `$table_name` (
		`id` INT NOT NULL auto_increment ,
		`is_active` INT NOT NULL DEFAULT '1' ,
		`content_is_php` INT NOT NULL DEFAULT '0' ,
		`top_right_width` VARCHAR (255) NOT NULL DEFAULT 'none',
		`top_left_width` VARCHAR (255) NOT NULL DEFAULT 'none',
		`name` VARCHAR (255) NOT NULL,
		`content` LONGTEXT,
		`shown_in`  LONGTEXT,
		`not_shown_in` LONGTEXT,
		`position` LONGTEXT,
		`shortcode` VARCHAR (255),
		`added_datetime` datetime,
		`updated_datetime` datetime,
		PRIMARY KEY ( `id` ))");
} else {// Add bing_url column
	$tmp_result = $wpdb->get_results("show columns from `$table_name` where field = 'top_right_width'");
	if (sizeof($tmp_result) == 0) { // No field created
		$wpdb->query("ALTER TABLE `$table_name` ADD `top_right_width` VARCHAR (255) NOT NULL");
	}
	unset($tmp_result);
	$tmp_result = $wpdb->get_results("show columns from `$table_name` where field = 'top_left_width'");
	if (sizeof($tmp_result) == 0) { // No field created
		$wpdb->query("ALTER TABLE `$table_name` ADD `top_left_width` VARCHAR (255) NOT NULL");
	}
}

