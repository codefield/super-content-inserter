<?php
/**
 * Include to show the list content
 *
 * @package admin-panel
 *
 */

// Security: Check if is admin user
SuperContentInserter_Users::an_admin_must_be_authenticated();

//Get all Contents
$contents = SuperContentInserter_Content::get_all();

// Convert position, shown_in and not_show_in to readable: header -> Header, pages::all_pages -> All pages ...
$super_content_insert_getall_list_content =  array();
$content_arr = array();
foreach ( $contents as $content ){
	$content_arr['id'] = $content->id;
	$content_arr['name'] = $content->name;
	$content_arr['shortcode'] = $content->shortcode;

	// Position
	$position_to_show = "";
	if ( $content->position ){
		$postions = explode( ';', $content->position );
		foreach ( $postions as $position ) {
			if ( 'init' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Init' 				: $position_to_show = $position_to_show . ', Init';
			} elseif ( 'header' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Header'				: $position_to_show = $position_to_show . ', Header';
			} elseif ( 'after_post_entry' == $position  ) {
				( '' == $position_to_show )? $position_to_show 		= 'After Post Entry' 	: $position_to_show = $position_to_show . ', After Post Entry';
			} elseif ( 'before_post_entry' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Before Post Entry' 	: $position_to_show = $position_to_show . ', Before Post Entry';
			} elseif ( 'post_entry_top_right' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Post Entry Top Right': $position_to_show = $position_to_show . ', Post Entry Top Right';
			} elseif ( 'post_entry_top_left' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Post Entry Top Left' : $position_to_show = $position_to_show . ', Post Entry Top Left';
			} elseif ( 'top_posts_sequence' == $position ) {
				( '' == $position_to_show )? $position_to_show 		= 'Top Posts Sequence'	: $position_to_show = $position_to_show . ', Top Posts Sequence';
			} elseif ( 'bottom_posts_sequence' == $position  ) {
				( '' == $position_to_show )? $position_to_show 		= 'Bottom Posts Sequence': $position_to_show = $position_to_show . ', Bottom Posts Sequence';
			} elseif ( 'footer' == $position  ) {
				( '' == $position_to_show )? $position_to_show 		= 'Footer'				: $position_to_show = $position_to_show . ', Footer';
			}
		}
	}
	// Save position
	$content_arr['position'] = $position_to_show;

	// Show in
	$show_in_to_show = '';
	if ( $content->shown_in ){
		$shown_in = explode( ';', $content->shown_in );
		if ( in_array( 'all', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All' 				: $show_in_to_show = $show_in_to_show . ', All';
		if ( in_array( 'home', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Home' 				: $show_in_to_show = $show_in_to_show . ', Home';
		if ( in_array( 'pages::all_pages', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Pages' 			: $show_in_to_show = $show_in_to_show . ', All Pages';
		elseif ( preg_match('/pages::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Pages' 		: $show_in_to_show = $show_in_to_show . ', Select Pages';
		if ( in_array( 'posts::all_posts', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Posts' 			: $show_in_to_show = $show_in_to_show . ', All Posts';
		elseif ( preg_match('/posts::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Posts' 		: $show_in_to_show = $show_in_to_show . ', Select Posts';
		if ( in_array( 'categories::all_categories', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Categories' 		: $show_in_to_show = $show_in_to_show . ', All Categories';
		elseif ( preg_match('/categories::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Categories' 	: $show_in_to_show = $show_in_to_show . ', Select Categories';
		if ( in_array( 'tags::all_tags', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Tags' 			: $show_in_to_show = $show_in_to_show . ', All Tags';
		elseif ( preg_match('/tags::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Tags' 		: $show_in_to_show = $show_in_to_show . ', Select Tags';

		if ( in_array( 'category_page::all_category_page', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Categories Page' : $show_in_to_show = $show_in_to_show . ', All Categories Page';
		elseif ( preg_match('/category_page::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Categories Page': $show_in_to_show = $show_in_to_show . ', Select Categories Page';

		if ( in_array( 'tags_page::all_tags_page', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'All Tags Page' 			: $show_in_to_show = $show_in_to_show . ', All Tags Page';
		elseif ( preg_match('/tags_page::/', $content->shown_in) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Select Tags Page'	: $show_in_to_show = $show_in_to_show . ', Select Tags Page';

		if ( in_array( 'archives', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= 'Archives' 			: $show_in_to_show = $show_in_to_show . ', Archives';
		if ( in_array( '404_page', $shown_in ) )
			( '' == $show_in_to_show )? $show_in_to_show 		= '404 Page' 			: $show_in_to_show = $show_in_to_show . ', 404 Page';
		}

	$content_arr['shown_in'] = $show_in_to_show;

	// Show in
	$not_show_in_to_show = '';
	if ( $content->not_shown_in ){
		$not_shown_in = explode( ';', $content->not_shown_in );
		if ( in_array( 'all', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'All' 				: $not_show_in_to_show = $not_show_in_to_show . ', All';
		if ( in_array( 'home', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Home' 				: $not_show_in_to_show = $not_show_in_to_show . ', Home';
		if ( in_array( 'pages::all_pages', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'All Pages' 			: $not_show_in_to_show = $not_show_in_to_show . ', All Pages';
		elseif ( preg_match('/pages::/', $content->not_shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Select Pages' 		: $not_show_in_to_show = $not_show_in_to_show . ', Select Pages';
		if ( in_array( 'posts::all_posts', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'All Posts' 			: $not_show_in_to_show = $not_show_in_to_show . ', All Posts';
		elseif ( preg_match('/posts::/', $content->not_shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Select Posts' 		: $not_show_in_to_show = $not_show_in_to_show . ', Select Posts';
		if ( in_array( 'categories::all_categories', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'All Categories' 		: $not_show_in_to_show = $not_show_in_to_show . ', All Categories';
		elseif ( preg_match('/categories::/', $content->not_shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Select Categories' 	: $not_show_in_to_show = $not_show_in_to_show . ', Select Categories';
		if ( in_array( 'tags::all_tags', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'All Tags' 			: $not_show_in_to_show = $not_show_in_to_show . ', All Tags';
		elseif ( preg_match('/tags::/', $content->not_shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Select Tags' 		: $not_show_in_to_show = $not_show_in_to_show . ', Select Tags';

		if ( in_array( 'category_page::all_category_page', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 				= 'All Categories Page' : $not_show_in_to_show = $not_show_in_to_show . ', All Categories Page';
		elseif ( preg_match('/category_page::/', $content->shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 				= 'Select Categories Page': $not_show_in_to_show = $not_show_in_to_show . ', Select Categories Page';

		if ( in_array( 'tags_page::all_tags_page', $shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 				= 'All Tags Page' 		: $not_show_in_to_show = $not_show_in_to_show . ', All Tags Page';
		elseif ( preg_match('/tags_page::/', $content->shown_in) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 				= 'Select Tags Page'	: $not_show_in_to_show = $not_show_in_to_show . ', Select Tags Page';

		if ( in_array( 'archives', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= 'Archives' 			: $not_show_in_to_show = $not_show_in_to_show . ', Archives';
		if ( in_array( '404_page', $not_shown_in ) )
			( '' == $not_show_in_to_show )? $not_show_in_to_show 		= '404 Page' 			: $not_show_in_to_show = $not_show_in_to_show . ', 404 Page';
	}
	$content_arr['not_shown_in'] = $not_show_in_to_show;

	$super_content_insert_getall_list_content[] =  $content_arr;
}

// Link to add page
$link_to_add_edit_content_page = admin_url('admin.php') . '?page=' . 'super-content-inserter-add-edit.php';

include( SuperContentInserter::$template_dir . '/includes/admin/content_list.php');
