// Prevent name collisions wrapping the code in an anonymous function.
jQuery(function ($) {
	/*
	 * Style selects.
	 */
	$('select').chosen();

	/*
	 * Style checkboxes.
	 */
	$('[type="checkbox"]').iCheckbox({
		switch_container_src: SCI.plugin_url + 'templates/js/lib/iCheckbox/images/switch-frame.png',
		class_container     : 'cf-checkbox-switcher-container',
		class_switch        : 'cf-checkbox-switch',
		class_checkbox      : 'cf-checkbox-checkbox',
		switch_speed        : 100,
		switch_swing        : -13
	});

	/*
	 * Checkboxes behaviour.
	 */
	/*
	 * Show select on check.
	 */
	// Initial behaviour.
	function checkboxes_behaviour(selector$) {
		if (selector$.is(':checked')) {
			selector$.closest('label').next('div').find('select').removeAttr('disabled');
			selector$.closest('label').next('div').show('fold');
		}
		else {
			selector$.closest('label').next('div').find('select').attr('disabled', 'disabled');
			selector$.closest('label').next('div').hide('fold');
		}

		selector$.closest('label').next('div').find('select').trigger('liszt:updated');
	}

	checkboxes_behaviour($('.checkbox_with_related_select'));

	$('.checkbox_with_related_select').on('change', function () {
		checkboxes_behaviour($(this));
	});

	/*
	 * Widths checkboxes behaviour.
	 */
	$('#position_post_entry_top_right, #position_post_entry_top_left').on('change', function () {
		if ($(this).is(':checked')) {
			$(this).closest('label').next().next('div').show('fold');
		}
		else {
			$(this).closest('label').next().next('div').hide('fold');
		}
	});

	/*
	 * All checkbox behaviour.
	 */
	$('#show_in_all,#not_show_in_all').on('change', function () {
		if ($(this).is(':checked')) {
			$(this).closest('td').find('[type="checkbox"]').not($(this)).removeAttr('checked').trigger('change');
		}
	});

	$('.show_in_checkbox').not('#show_in_all').on('change', function () {
		if ($(this).is(':checked')) {
			$('#show_in_all').removeAttr('checked').trigger('change');
		}
	});

	$('.not_show_in_checkbox').not('#not_show_in_all').on('change', function () {
		if ($(this).is(':checked')) {
			$('#not_show_in_all').removeAttr('checked').trigger('change');
		}
	});

	/*
	 * Ajax Form Declaration.
	 */
	$('.cf-ajax-form').ajaxForm({
		beforeSubmit: function (form_data_arr, form$, options) {
			/*
			 * Used to disable the button and show the loader.
			 */
			$('.cf-submit-tr img', form$).show();
			//$('.cf-submit-tr button[type="submit"]', form$).button('disable');
		},
		data        : {
			action: 'add'
		},
		dataType    : 'json',
		error       : function (a, b, c) {
			/*
			 * Don't show the message in case that the user cancel the query.'
			 */
			if (c) {
				/*
				 * Remove ajax loader and disable button.
				 */
				$('.cf-submit-tr img').hide();
				//$('.cf-submit-tr button[type="submit"]').button('enable');

				// Clear message dashboard.
				$('#cf-message-container').html('');
				$('#message').remove();

				$('#cf-templates-container .cf-error-message .cf-msg-mark').html(b + ': ' + c);
				$('#cf-templates-container .cf-error-message').clone().appendTo('#cf-message-container');

				$('.cf-error-message').effect('highlight', {
					color: '#FF655D'
				}, 1000, function () {
				});
				$('.cf-notification-message').effect('highlight', {
					color: '#BDD1B5'
				}, 1000, function () {
				});

				$.scrollTo(0, 800, {
					easing: 'swing'
				});
			}
		},
		success     : function (response_from_server, statusText, xhr, form$) {
			/*
			 * Remove ajax loader and show button.
			 */
			$('.cf-submit-tr img', form$).hide();
			//$('.cf-submit-tr button[type="submit"]', form$).button('enable');

			// Clear message dashboard.
			$('#cf-message-container').html('');
			$('#message').remove();

			if (response_from_server.type == 'notification') {
				/*
				 * Redirect to Edit shortcode.
				 */
				if (response_from_server.hasOwnProperty('data')
					&& response_from_server.data.hasOwnProperty('redirect_url')
					&& response_from_server.data.redirect_url != null) {
					// Is an addition so redirect.
					window.location = response_from_server.data.redirect_url;
				}
				else {
					/*
					 * Show server message to user.
					 */
					$('#cf-templates-container .cf-notification-message .cf-msg-mark').html(response_from_server.message);
					$('#cf-templates-container .cf-notification-message').clone().appendTo('#cf-message-container');

					$('.cf-error-message').effect('highlight', {
						color: '#FF655D'
					}, 1000, function () {
					});
					$('.cf-notification-message').effect('highlight', {
						color: '#BDD1B5'
					}, 1000, function () {
					});

					$.scrollTo(0, 800, {
						easing: 'swing'
					});
				}
			}
			else if (response_from_server.type == 'error') {
				/*
				 * Show server message to user.
				 */
				$('#cf-templates-container .cf-error-message .cf-msg-mark').html(response_from_server.message);
				$('#cf-templates-container .cf-error-message').clone().appendTo('#cf-message-container');

				$('.cf-error-message').effect('highlight', {
					color: '#FF655D'
				}, 1000, function () {
				});
				$('.cf-notification-message').effect('highlight', {
					color: '#BDD1B5'
				}, 1000, function () {
				});

				$.scrollTo(0, 800, {
					easing: 'swing'
				});
			}
		},
		type        : 'POST',
		url         : ajaxurl
	});
});