// Prevent name collisions wrapping the code in an anonymous function.
jQuery(function ($) {
	/*
	 * DataTables.
	 */
	var oTable = $('#content-blocks-list').dataTable({
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		},
		"bAutoWidth": false
	});

	/*
	 * Get the rows which are currently selected.
	 */
	function fnGetSelected(oTableLocal) {
		return oTableLocal.$('tr#' + $('#content_to_delete').val());
	}

	$('.actions-column .button-secondary').on('click', function () {
		$('#content_to_delete').val($(this).closest('tr').attr('id'));
	});

	$('#thickbox-dialog .button-primary').on('click', function () {
		var button$ = $(this);

		$.ajax({
			beforeSend: function () {
				/*
				 * Used to disable the button and show the loader.
				 */
				$('.cf-ajax-loader-container img').show();
			},
			data: {
				action: 'del',
				submit_delete: '',
				id: $('#content_to_delete').val()
			},
			dataType: 'json',
			error: function (a, b, c) {
				/*
				 * Don't show the message in case that the user cancel the query.'
				 */
				if (c) {
					/*
					 * Remove ajax loader and disable button.
					 */
					$('.cf-ajax-loader-container img').hide();
					//$('.cf-submit-tr button[type="submit"]').button('enable');

					// Clear message dashboard.
					$('#cf-message-container').html('');

					$('#cf-templates-container .cf-error-message .cf-msg-mark').html(b + ': ' + c);
					$('#cf-templates-container .cf-error-message').clone().appendTo('#cf-message-container');

					$('.cf-error-message').effect('highlight', {
						color: '#FF655D'
					}, 1000, function () {
					});
					$('.cf-notification-message').effect('highlight', {
						color: '#BDD1B5'
					}, 1000, function () {
					});

					$.scrollTo(0, 800, {
						easing: 'swing'
					});
				}
			},
			success: function (response_from_server, statusText, xhr, form$) {
				/*
				 * Remove ajax loader and show button.
				 */
				$('.cf-ajax-loader-container img').hide();
				//$('.cf-submit-tr button[type="submit"]', form$).button('enable');

				// Clear message dashboard.
				$('#cf-message-container').html('');

				tb_remove();
				if (response_from_server.type == 'notification') {
					/*
					 * Show server message to user.
					 */
					$('#cf-templates-container .cf-notification-message .cf-msg-mark').html(response_from_server.message);
					$('#cf-templates-container .cf-notification-message').clone().appendTo('#cf-message-container');

					var anSelected = fnGetSelected(oTable);
					if (anSelected.length !== 0) {
						oTable.fnDeleteRow(anSelected[0]);
					}
				}
				else if (response_from_server.type == 'error') {
					/*
					 * Show server message to user.
					 */
					$('#cf-templates-container .cf-error-message .cf-msg-mark').html(response_from_server.message);
					$('#cf-templates-container .cf-error-message').clone().appendTo('#cf-message-container');
				}

				$('.cf-error-message').effect('highlight', {
					color: '#FF655D'
				}, 1000, function () {
				});
				$('.cf-notification-message').effect('highlight', {
					color: '#BDD1B5'
				}, 1000, function () {
				});

				$.scrollTo(0, 800, {
					easing: 'swing'
				});
			},
			type: 'POST',
			url: ajaxurl
        });
	});

	$('#thickbox-dialog .button-secondary').on('click', function () {
		tb_remove();
	});
});