<?php
/**
 * Template to Help page.
 */
?>

<div class="wrap cf-page">
	<div class="icon32" id="icon-options-general"><br /></div>
	<h2 class="cf-page-header">
		<?php _e('Help', SuperContentInserter::$i18n_prefix)?>
		<br />
		<span class="description"><?php _e('In this page you will get all the help you will need to work with the plugin.', SuperContentInserter::$i18n_prefix)?></span>
	</h2>

	<div id="cf-message-container">
		<?php include( SuperContentInserter::$template_dir . '/includes/msg.php' ); ?>
	</div>

	<p>
		<strong><em><?php _e('Coming soon section...', SuperContentInserter::$i18n_prefix)?></em></strong>
	</p>
</div>