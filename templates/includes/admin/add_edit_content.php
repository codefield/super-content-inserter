<?php
/**
 * Template to add the Content.
 *
 * @uses	int			$is_editing			1 is editing, if 0 is adding.
 * @uses	string		$link_to_list_content_page		Link to the listing page.
 * @uses	string		$data['shortcode']	Generated shortcode for the content block.
 *
 * @uses	int			$content_id			Id of the content block if is in editing mode. 0 if in adding mode.
 *
 * @uses	array		$all_pages			All pages.
 * @uses	int			$all_pages->id		All pages.
 * @uses	string		$all_pages->name	All pages.
 *
 * @uses	array		$all_posts			All posts.
 *
 * @uses	array		$all_categories		All categories.
 *
 * @uses	array		$all_tags			All tags.
 *
 *
 * Main section.
 *
 * @uses	string		$name				Content block name.
 * @uses	string		$content_text		Content of the block.
 * @uses	string		$content_is_php		1 is checked, 0 if not.
 *
 * Position section.
 *
 * @uses	Checkbox	$position_header				1 is checked, 0 if not.
 * @uses	Checkbox	$position_footer				1 is checked, 0 if not.
 * @uses	Checkbox	$position_after_post_entry		1 is checked, 0 if not.
 * @uses	Checkbox	$position_before_post_entry		1 is checked, 0 if not.
 *
 * @uses	Checkbox	$position_post_entry_top_right	1 is checked, 0 if not.
 * @uses	int			$top_right_width				The value it will be considered for the content block. Default: ''.
 *
 * @uses	Checkbox	$position_post_entry_top_left	1 is checked, 0 if not.
 * @uses	int			$top_left_width					The value it will be considered for the content block. Default: ''.
 *
 * @uses	Checkbox	$position_top_posts_sequence	1 is checked, 0 if not.
 * @uses	Checkbox	$position_bottom_posts_sequence	1 is checked, 0 if not.
 *
 * "Show in" section.
 *
 * @uses	Checkbox	$show_in_all					1 is checked, 0 if not.
 * @uses	Checkbox	$show_in_home					1 is checked, 0 if not.
 *
 * @uses	Checkbox	$show_in_specific_pages			1 is checked, 0 if not.
 * @uses	array		$show_in_select_pages_selected	Array of selected Pages id.
 *
 * @uses	Checkbox	$show_in_specific_posts			1 is checked, 0 if not.
 * @uses	array		$show_in_select_posts_selected	Array of selected Posts id.
 *
 * @uses	Checkbox	$show_in_specific_categories	1 is checked, 0 if not.
 * @uses	array		$show_in_select_categories_selected			Array of selected Categories id.
 *
 * @uses	Checkbox	$show_in_specific_tags			1 is checked, 0 if not.
 * @uses	array		$show_in_select_tags_selected	Array of selected Tags id.
 *
 * @uses	Checkbox	$show_in_archives				1 is checked, 0 if not.
 *
 * @uses	Checkbox	$show_in_category_page			1 is checked, 0 if not.
 * @uses	array		$show_in_specific_categories_page_selected 		Array of selected Categories id.
 *
 * @uses	Checkbox	$show_in_tags_page				1 is checked, 0 if not.
 * @uses	array		$show_in_specific_tags_page_selected 		Array of selected Tags id.
 *
 * @uses	Checkbox	$show_in_404_page				1 is checked, 0 if not.
 *
 * "Not show in" section.
 *
 * @uses	Checkbox	$not_show_in_all					1 is checked, 0 if not.
 * @uses	Checkbox	$not_show_in_home					1 is checked, 0 if not.
 *
 * @uses	Checkbox	$not_show_in_specific_pages			1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_pages_selected	Array of selected Pages id.
 *
 * @uses	Checkbox	$not_show_in_specific_posts			1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_posts_selected	Array of selected Posts id.
 *
 * @uses	Checkbox	$not_show_in_specific_categories	1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_categories_selected			Array of selected Categories id.
 *
 * @uses	Checkbox	$not_show_in_specific_tags			1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_tags_selected	Array of selected Tags id.
 *
 * @uses	Checkbox	$not_show_in_archives				1 is checked, 0 if not.
 * @uses	Checkbox	$not_show_in_category_page			1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_categories_selected			Array of selected Categories id.
 *
 * @uses	Checkbox	$not_show_in_tags_page				1 is checked, 0 if not.
 * @uses	array		$not_show_in_select_tags_selected	Array of selected Tags id.
 *
 * @uses	Checkbox	$not_show_in_404_page				1 is checked, 0 if not.
 *
 * @uses	Submit		submit_add_content				Add new content block.
 *
 */
?>

<div class="wrap cf-page">
    <div class="icon32" id="icon-options-general"><br /></div>
	<h2 class="cf-page-header">
		<?php ( $is_editing )?_e('Edit Content',SuperContentInserter::$i18n_prefix):_e('Add New Content',SuperContentInserter::$i18n_prefix)?>
		<a class="add-new-h2" href="<?php echo $link_to_list_content_page; ?>"><?php _e('← Back to Main', SuperContentInserter::$i18n_prefix)?></a>
		<br />
		<span class="description">
			<?php ( $is_editing )?_e('This page allows you to edit a content block.',SuperContentInserter::$i18n_prefix):_e('This page allows you to add new content blocks.',SuperContentInserter::$i18n_prefix)?>
		</span>
	</h2>

	<div id="cf-message-container">
		<?php include( SuperContentInserter::$template_dir . '/includes/msg.php'); ?>
	</div>

	<form class="cf-ajax-form" action="" method="post">

		<?php wp_nonce_field('SuperContentInserter-add-update-content');?>

		<input type="hidden" name="content_id" id="content_id" value="<?php
			if ($is_editing) {
				echo $content_id;
			}
			else {
				echo '0';
			}?>" />

		<table class="form-table select-fields">
			<tbody>
				<tr vlalign="top">
					<th scope="row" nowrap="nowrap">
						<label for="name"><?php _e('Name',SuperContentInserter::$i18n_prefix)?></label>
					</th>
					<td>
						<input type="text" id="name" class="regular-text" name="name" value="<?php if ($is_editing) { echo $data['name'];}?>" />
						<br />
						<label for="name">
							<span class="description"><?php _e(' Content block identifier.',SuperContentInserter::$i18n_prefix)?></span>
						</label>
					</td>
				</tr>

				<?php if ($is_editing) { ?>
					<tr vlalign="top">
						<th scope="row" nowrap="nowrap">
							<label for="name"><?php _e('Shortcode',SuperContentInserter::$i18n_prefix)?></label>
						</th>
						<td>
							<span class="description"><?php echo $data['shortcode']; ?></span>
						</td>
					</tr>
				<?php } ?>

				<tr vlalign="top">
					<th scope="row" nowrap="nowrap">
						<label for="content"><?php _e('Content',SuperContentInserter::$i18n_prefix)?></label>
					</th>

					<td>
						<?php
							if ($is_editing)
								wp_editor(stripslashes($data['content_text']),'content_text',$settings = array('media_buttons' => FALSE));
							else
								wp_editor('','content_text', $settings = array( 'media_buttons' => FALSE));
						?>

						<br />
						<label>
							<input type="checkbox" id="content_is_php"  name="content_is_php" value="1" <?php if ($is_editing && $data['content_is_php'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Check if the content is PHP code.',SuperContentInserter::$i18n_prefix)?></span>
						</label>
					</td>
				</tr>

				<tr vlalign="top">
					<th scope="row" nowrap="nowrap">
						<label for="position_header"><?php _e('Position',SuperContentInserter::$i18n_prefix)?></label>
					</th>

					<td>
						<label>
							<input type="checkbox" id="position_header" name="position_header" value="1" <?php if ($is_editing && isset($data['position_header']) && $data['position_header'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Header.',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" id="position_footer"  name="position_footer" value="1" <?php if ($is_editing && isset($data['position_footer']) && $data['position_footer'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Footer.',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />
						<br />

						<label>
							<input type="checkbox" id="position_after_post_entry" name="position_after_post_entry" value="1" <?php if ($is_editing && isset($data['position_after_post_entry']) && $data['position_after_post_entry'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<strong><?php _e('After',SuperContentInserter::$i18n_prefix)?></strong>&nbsp;<?php _e('Post Entry.',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" id="position_before_post_entry"  name="position_before_post_entry" value="1" <?php if ($is_editing && isset($data['position_before_post_entry']) && $data['position_before_post_entry'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<strong><?php _e('Before',SuperContentInserter::$i18n_prefix)?></strong>&nbsp;<?php _e('Post Entry',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" id="position_post_entry_top_right" class="checkbox_with_related_select" name="position_post_entry_top_right" value="1" <?php if ($is_editing && isset($data['position_post_entry_top_right']) && $data['position_post_entry_top_right'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Post Entry',SuperContentInserter::$i18n_prefix)?>&nbsp;<strong><?php _e('Top Right.',SuperContentInserter::$i18n_prefix)?></strong></span>
						</label>

						<div class="select-container">
							<label for="top_right_width">
								<span class="description"><?php _e('Enter the content width: ',SuperContentInserter::$i18n_prefix)?></span>
								<input type="text" id="top_right_width" class="small-text text-align-right" name="top_right_width" value="<?php if ($is_editing && isset($data['top_right_width']) && $data['top_right_width'] != '' ) {  echo $data['top_right_width']; } else { echo ""; }?>" />
								<span class="description"><?php _e('px',SuperContentInserter::$i18n_prefix)?></span>
								<br />
								<span class="description"><?php _e(' If empty then an "" value will be considered.',SuperContentInserter::$i18n_prefix)?></span>
							</label>
						</div>

						<br />

						<label>
							<input type="checkbox" id="position_post_entry_top_left" class="checkbox_with_related_select" name="position_post_entry_top_left" value="1" <?php if ($is_editing && isset($data['position_post_entry_top_left']) && $data['position_post_entry_top_left'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Post Entry',SuperContentInserter::$i18n_prefix)?>&nbsp;<strong><?php _e('Top Left.',SuperContentInserter::$i18n_prefix)?></strong></span>
						</label>

						<div class="select-container">
							<label for="top_left_width">
								<span class="description"><?php _e('Enter the content width: ',SuperContentInserter::$i18n_prefix)?></span>
								<input type="text" id="top_left_width" class="small-text text-align-right" name="top_left_width" value="<?php if ($is_editing && isset($data['top_left_width']) && $data['top_left_width'] != '' ) {  echo $data['top_left_width']; } else { echo ""; }?>" />
								<span class="description"><?php _e('px',SuperContentInserter::$i18n_prefix)?></span>
								<br />
								<span class="description"><?php _e(' If empty then an "" value will be considered.',SuperContentInserter::$i18n_prefix)?></span>
							</label>
						</div>

						<br />

						<label>
							<input type="checkbox" id="position_top_posts_sequence" name="position_top_posts_sequence" value="1" <?php if ($is_editing && isset($data['position_top_posts_sequence']) && $data['position_top_posts_sequence'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<strong><?php _e('Top',SuperContentInserter::$i18n_prefix)?></strong>&nbsp;<?php _e('of Posts Sequence.',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" id="position_bottom_posts_sequence" name="position_bottom_posts_sequence" value="1" <?php if ($is_editing && isset($data['position_bottom_posts_sequence']) && $data['position_bottom_posts_sequence'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<strong><?php _e('Bottom',SuperContentInserter::$i18n_prefix)?></strong>&nbsp;<?php _e('of Posts Sequence.',SuperContentInserter::$i18n_prefix)?></span>
						</label>
					</td>
				</tr>

				<tr class="cf-separator"></tr>

				<tr vlalign="top">
					<th scope="row" nowrap="nowrap">
						<label for="show_in_all"><?php _e('Where <strong>To Show</strong> The Content',SuperContentInserter::$i18n_prefix)?></label>
					</th>
					<td>
						<label>
							<input type="checkbox" class="show_in_checkbox" id="show_in_all" name="show_in_all" value="1" checked="checked" <?php if ($is_editing && isset($data['show_in_all']) && $data['show_in_all'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('All',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" class="show_in_checkbox" id="show_in_home" name="show_in_home" value="1" <?php if ($is_editing && isset($data['show_in_home']) && $data['show_in_home'] == 1) { echo 'checked="checked"';} ?> />
							<span class="description">&nbsp;<?php _e('Home',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />
						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select show_in_checkbox" id="show_in_specific_pages" name="show_in_specific_pages" value="1" <?php if ($is_editing && isset($data['show_in_specific_pages']) && $data['show_in_specific_pages'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Pages',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Pages...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_pages_selected" name="show_in_specific_pages_selected[]" multiple="multiple" disabled="disabled">
								<?php foreach ($all_pages as $all_pages_item) { ?>
									<option value="<?php echo $all_pages_item->ID;?>" <?php echo ($is_editing && isset( $data['show_in_specific_pages_selected'] ) && in_array($all_pages_item->ID,$data['show_in_specific_pages_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_pages_item->post_title;?>
									</option>
								<?php } ?>
							</select>
							<br />
							<span class="description">
								<?php _e('If no page is selected then all pages will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>
						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select show_in_checkbox" id="show_in_specific_posts" name="show_in_specific_posts" value="1" <?php if ($is_editing && $data['show_in_specific_posts'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Posts',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Posts...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_posts_selected" name="show_in_specific_posts_selected[]" multiple="multiple"  disabled="disabled" <?php echo ($is_editing && isset($data['show_in_specific_posts_selected']) && in_array($all_pages_item->ID,$data['show_in_specific_posts_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_posts as $all_posts_item) { ?>
									<option value="<?php echo $all_posts_item->ID;?>" <?php echo ($is_editing && isset($data['show_in_specific_posts_selected']) && isset($data['show_in_specific_posts_selected']) && '' != $data['show_in_specific_posts_selected'] && in_array($all_posts_item->ID,$data['show_in_specific_posts_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_posts_item->post_title;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no post is selected then all posts will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select show_in_checkbox" id="show_in_specific_categories" name="show_in_specific_categories" value="1" <?php if ($is_editing && $data['show_in_specific_categories'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Categories',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Categories...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_categories_selected" name="show_in_specific_categories_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['show_in_specific_categories_selected'] ) echo ($is_editing && in_array($all_categories_item->term_id,$data['show_in_specific_categories_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_categories as $all_categories_item) { ?>
									<option value="<?php echo $all_categories_item->term_id;?>"<?php echo ($is_editing && isset( $data['show_in_specific_categories_selected'] ) && in_array($all_categories_item->term_id,$data['show_in_specific_categories_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_categories_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no category is selected then all categories will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select show_in_checkbox" id="show_in_specific_tags" name="show_in_specific_tags" value="1" <?php if ($is_editing && $data['show_in_specific_tags'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Tags',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Tags...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_tags_selected" name="show_in_specific_tags_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['show_in_specific_tags_selected'] ) echo ($is_editing && in_array($all_tags_item->term_id,$data['show_in_specific_tags_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_tags as $all_tags_item) { ?>
									<option value="<?php echo $all_tags_item->term_id;?>"<?php echo ($is_editing && isset( $data['show_in_specific_tags_selected'] ) && in_array($all_tags_item->term_id,$data['show_in_specific_tags_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_tags_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no tag is selected then all tags will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />
						<br />

						<label>
							<input type="checkbox" class="show_in_checkbox" id="show_in_archives" name="show_in_archives" value="1" <?php if ($is_editing && isset( $data['show_in_archives'] ) && $data['show_in_archives'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Archives',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" class="show_in_checkbox checkbox_with_related_select" id="show_in_category_page" name="show_in_category_page" value="1" <?php if ($is_editing && isset( $data['show_in_category_page'] ) && $data['show_in_category_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Categories Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Categories...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_categories_page_selected" name="show_in_specific_categories_page_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['show_in_specific_categories_page_selected'] ) echo ($is_editing && in_array($all_categories_item->term_id,$data['show_in_specific_categories_page_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_categories as $all_categories_item) { ?>
									<option value="<?php echo $all_categories_item->term_id;?>"<?php echo ($is_editing && isset( $data['show_in_specific_categories_page_selected'] ) && in_array($all_categories_item->term_id,$data['show_in_specific_categories_page_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_categories_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no category is selected then all categories will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="show_in_checkbox checkbox_with_related_select" id="show_in_tags_page" name="show_in_tags_page" value="1" <?php if ($is_editing && isset( $data['show_in_tags_page'] ) && $data['show_in_tags_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Tags Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Tags...',SuperContentInserter::$i18n_prefix)?>" id="show_in_specific_tags_page_selected" name="show_in_specific_tags_page_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['show_in_specific_tags_page_selected'] ) echo ($is_editing && in_array($all_tags_item->term_id,$data['show_in_specific_tags_page_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_tags as $all_tags_item) { ?>
									<option value="<?php echo $all_tags_item->term_id;?>"<?php echo ($is_editing && isset( $data['show_in_specific_tags_page_selected'] ) && in_array($all_tags_item->term_id,$data['show_in_specific_tags_page_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_tags_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no tag is selected then all tags will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="show_in_checkbox" id="show_in_404_page" name="show_in_404_page" value="1" <?php if ($is_editing && isset( $data['show_in_404_page'] ) && $data['show_in_404_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('404 Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>
					</td>
				</tr>

				<tr class="cf-separator"></tr>

				<tr vlalign="top">
					<th scope="row" nowrap="nowrap">
						<label for="not_show_in_all"><?php _e('Where <strong>Not To Show</strong> The Content',SuperContentInserter::$i18n_prefix)?></label>
					</th>
					<td>
						<label>
							<input type="checkbox" class="not_show_in_checkbox" id="not_show_in_all" name="not_show_in_all" value="1" <?php if ($is_editing && isset( $data['not_show_in_all'] ) && $data['not_show_in_all'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('All',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" class="not_show_in_checkbox" id="not_show_in_home" name="not_show_in_home" value="1" <?php if ($is_editing && isset( $data['not_show_in_home'] ) && $data['not_show_in_home'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Home',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />
						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select not_show_in_checkbox" id="not_show_in_specific_pages" name="not_show_in_specific_pages" value="1" <?php if ($is_editing && isset( $data['not_show_in_specific_pages'] ) && $data['not_show_in_specific_pages'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Pages',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Pages...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_pages_selected" name="not_show_in_specific_pages_selected[]" multiple="multiple" disabled="disabled" <?php echo ($is_editing && isset( $data['not_show_in_specific_pages_selected'] ) && '' != $data['not_show_in_specific_pages_selected'] && in_array($all_pages_item->ID,$data['not_show_in_specific_pages_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_pages as $all_pages_item) { ?>
									<option value="<?php echo $all_pages_item->ID;?>"<?php echo ($is_editing && isset( $data['not_show_in_specific_pages_selected']  ) && '' != $data['not_show_in_specific_pages_selected'] && in_array($all_pages_item->ID,$data['not_show_in_specific_pages_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_pages_item->post_title;?>
									</option>
								<?php } ?>
							</select>
							<br />
							<span class="description">
								<?php _e('If no page is selected then all pages will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>
						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select not_show_in_checkbox" id="not_show_in_specific_posts" name="not_show_in_specific_posts" value="1" <?php if ($is_editing && isset( $data['not_show_in_specific_posts'] ) && $data['not_show_in_specific_posts'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Posts',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Posts...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_posts_selected" name="not_show_in_specific_posts_selected[]" multiple="multiple"  disabled="disabled" <?php echo ($is_editing && '' != $data['not_show_in_specific_posts_selected'] && in_array($all_pages_item->ID,$data['not_show_in_specific_posts_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_posts as $all_posts_item) { ?>
									<option value="<?php echo $all_posts_item->ID;?>" <?php echo ($is_editing && isset( $data['not_show_in_specific_posts_selected'] ) && '' != $data['not_show_in_specific_posts_selected'] && in_array($all_posts_item->ID,$data['not_show_in_specific_posts_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_posts_item->post_title;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no post is selected then all posts will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select not_show_in_checkbox" id="not_show_in_specific_categories" name="not_show_in_specific_categories" value="1"  <?php if ($is_editing && isset( $data['not_show_in_specific_categories'] ) && $data['not_show_in_specific_categories'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Categories',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Categories...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_categories_selected" name="not_show_in_specific_categories_selected[]" multiple="multiple"  disabled="disabled" <?php if ( isset( $data['not_show_in_specific_categories_selected'] ) && 'ARRAY' === $data['not_show_in_specific_categories_selected'] ) echo ($is_editing && in_array($all_categories_item->term_id,$data['not_show_in_specific_categories_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_categories as $all_categories_item) { ?>
									<option value="<?php echo $all_categories_item->term_id;?>" <?php echo ($is_editing && isset($data['not_show_in_specific_categories_selected']) && '' != $data['not_show_in_specific_categories_selected'] && in_array($all_categories_item->term_id,$data['not_show_in_specific_categories_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_categories_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no category is selected then all categories will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="checkbox_with_related_select not_show_in_checkbox" id="not_show_in_specific_tags" name="not_show_in_specific_tags" value="1" <?php if ($is_editing && isset( $data['not_show_in_specific_tags'] ) && $data['not_show_in_specific_tags'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Specific Tags',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Tags...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_tags_selected" name="not_show_in_specific_tags_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['not_show_in_specific_tags_selected'] ) echo ($is_editing && in_array($all_tags_item->term_id,$data['not_show_in_specific_tags_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_tags as $all_tags_item) { ?>
									<option value="<?php echo $all_tags_item->term_id;?>" <?php echo ($is_editing && isset( $data['not_show_in_specific_tags_selected'] ) && in_array($all_tag_item->term_id,$data['not_show_in_specific_tags_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_tags_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no tag is selected then all tags will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />
						<br />

						<label>
							<input type="checkbox" class="not_show_in_checkbox" id="not_show_in_archives" name="not_show_in_archives" value="1" <?php if ($is_editing && $data['not_show_in_archives'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Archives',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<br />

						<label>
							<input type="checkbox" class="not_show_in_checkbox checkbox_with_related_select" id="not_show_in_category_page" name="not_show_in_category_page" value="1" <?php if ($is_editing && $data['not_show_in_category_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Categories Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Categories...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_categories_page_selected" name="not_show_in_specific_categories_page_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['not_show_in_specific_categories_page_selected'] ) echo ($is_editing && in_array($all_categories_item->term_id,$data['not_show_in_specific_categories_page_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_categories as $all_categories_item) { ?>
									<option value="<?php echo $all_categories_item->term_id;?>"<?php echo ($is_editing && isset( $data['not_show_in_specific_categories_page_selected'] ) && in_array($all_categories_item->term_id,$data['not_show_in_specific_categories_page_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_categories_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no category is selected then all categories will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="not_show_in_checkbox checkbox_with_related_select" id="not_show_in_tags_page" name="not_show_in_tags_page" value="1" <?php if ($is_editing && $data['not_show_in_tags_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('Tags Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>

						<div class="select-container">
							<select data-placeholder="<?php _e('Select some Tags...',SuperContentInserter::$i18n_prefix)?>" id="not_show_in_specific_tags_page_selected" name="not_show_in_specific_tags_page_selected[]" multiple="multiple"  disabled="disabled" <?php if ( 'ARRAY' === $data['not_show_in_specific_tags_page_selected'] ) echo ($is_editing && in_array($all_tags_item->term_id,$data['not_show_in_specific_tags_page_selected']))?'selected="selected"':''; ?>>
								<?php foreach ($all_tags as $all_tags_item) { ?>
									<option value="<?php echo $all_tags_item->term_id;?>" <?php echo ($is_editing && isset( $data['not_show_in_specific_tags_page_selected'] ) && in_array($all_tags_item->term_id,$data['not_show_in_specific_tags_page_selected']))?'selected="selected"':''; ?>>
										<?php echo $all_tags_item->name;?>
									</option>
								<?php }?>
							</select>

							<br />
							<span class="description">
								<?php _e('If no tag is selected then all tags will be considered.',SuperContentInserter::$i18n_prefix)?>
							</span>
						</div>

						<br />

						<label>
							<input type="checkbox" class="not_show_in_checkbox" id="not_show_in_404_page" name="not_show_in_404_page" value="1" <?php if ($is_editing && $data['not_show_in_404_page'] == 1) { echo 'checked="checked"';}?> />
							<span class="description">&nbsp;<?php _e('404 Page',SuperContentInserter::$i18n_prefix)?></span>
						</label>
					</td>
				</tr>

				<tr class="cf-separator"></tr>

				<tr class="cf-submit-tr">
					<th></th>
					<td>
						<button type="submit" class="button-primary" name="<?php echo ($is_editing)?'submit_update_content':'submit_add_content'?>">
							<?php ($is_editing)?_e('Update Content Block',SuperContentInserter::$i18n_prefix):_e('Add Content Block',SuperContentInserter::$i18n_prefix)?>
						</button>

						<span class="cf-ajax-loader-container">
							<img src="<?php echo get_bloginfo ('wpurl') . '/wp-admin/images/wpspin_light.gif'; ?>" class="cf-ajax-loader" alt="Loading..." title="Loading data...">
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</form>

	<div id="cf-templates-container" class="display-none">
		<div class="cf-error-message ui-state-error ui-corner-all cf-negative cf-position-relative" style="padding: 0 0.7em;">
			<p>
				<span class="cf-icon-suggestion-type"></span>
				<span class="cf-msg-mark"></span>
			</p>
		</div>
		<div class="cf-notification-message ui-state-highlight ui-corner-all cf-positive cf-position-relative" style="padding: 0 0.7em;">
			<span class="cf-icon-suggestion-type"></span>
			<p>
				<span class="cf-msg-mark"></span>
			</p>
		</div>
	</div>
</div>
