<?php
/*
Plugin Name: Super Content Inserter
Version: 0.2
Description: Add and display Content into blog content in specific sections.
*/

/**
 * WordPress Version.
 * @global	string	$wp_version
 */
global $wp_version;

// Avoid name collisions.
if (!class_exists('SuperContentInserter')) {
	class SuperContentInserter
	{
		/**
		 * The url to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_url;

		/**
		 * The path to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_dir;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $template_dir;

		/**
		 * The prefix for the plugin tables in database
		 *
		 * @static
		 * @var string
		 */
		static $db_prefix;

		/**
		 * The prefix for the plugin internationalization
		 *
		 * @static
		 * @var string
		 */
		static $i18n_prefix;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var content
		 */
		private $content;

		/**
		 * Executes all initialization code for the plugin.
		 *
		 * @access public
		 */
		function __construct() {

			// Define static values
			self::$plugin_url = trailingslashit( WP_PLUGIN_URL.'/'. dirname( plugin_basename(__FILE__) ));
			self::$plugin_dir = trailingslashit( WP_PLUGIN_DIR.'/'. dirname( plugin_basename(__FILE__) ));
			self::$template_dir = self::$plugin_dir . '/templates';
			self::$db_prefix = 'super_content_inserter_';
			self::$i18n_prefix = 'super_content_inserter';

			// Include all classes
			include(self::$plugin_dir . '/includes/all_classes.php');

			// Add AJAX support through native WP admin-ajax action.
			add_action('wp_ajax_add', array(&$this, 'ajax_add'));
			add_action('wp_ajax_del', array(&$this, 'ajax_del'));

			// Add Menu Box in Admin Panel
			add_action('admin_menu', array(&$this, 'admin_menu'));

			add_action('init', array( &$this, 'register_shortcode' ) );

			add_action('init', array(&$this, 'super_content_inserter_display_content'), 100);

			// Update box suggestions data on rich text editor changes.
			add_filter('tiny_mce_before_init', array(&$this, 'update_rte'));
		}

		/**
		 * Handles the AJAX action for add objects.
		 *
		 */
		function ajax_add() {
			// This is how you get access to the database.
			global $wpdb;

			include(self::$plugin_dir . '/pages/ajax/add_data.php');
		}

		/**
		 * Handles the AJAX action for delete objects.
		 *
		 */
		function ajax_del() {
			// This is how you get access to the database.
			global $wpdb;

			include(self::$plugin_dir . '/pages/ajax/delete_data.php');
		}

		/**
		 * Update textarea on rich text editor changes.
		 */
		function update_rte($initArray){
			$initArray['setup'] = <<<JS
[function(ed) {
    ed.onChange.add(function(ed, e) {
		// Update HTML view textarea (that is the one used to send the data to server).
		ed.save();
	});
}][0]
JS;

			return $initArray;
		}

		function register_shortcode(){
			add_shortcode( 'sci' , array( &$this, 'display_shortcode'), -1000);
		}

		function display_shortcode($atts){

			// get content by id and chequed if it is shown
			if ( $content = SuperContentInserter_Content::get( $atts['id'] ) and '1' == $content->is_active ){
				if ( '1' == $content->content_is_php ){
					ob_start();
					eval($content->content);
					$eval_result = ob_get_contents();
					ob_end_clean();
					$value =  do_shortcode( $eval_result );
				} else {
					$value = do_shortcode( $content->content );
				}
				return $value;
			}
		}

		function super_content_inserter_display_content() {

			// TODO: Get all content from db the position and call function
			$contents = SuperContentInserter_Content::get_all();

			foreach ( $contents as $content ){
				// Check if content is active
				if ( '1' == $content->is_active ){
					$position = explode( ';' ,$content->position );
					if ( in_array( 'header', $position ) ){
						$this->content['header'][] = $content;
						add_filter( 'wp_head', array( &$this, 'display_content_header' ));
					}
					if (  in_array( 'after_post_entry', $position )  ){
						$this->content['after_post_entry'][] = $content;
					}
					if (in_array( 'before_post_entry', $position ) ){
						$this->content['before_post_entry'][] = $content;
					}
					if (  in_array( 'post_entry_top_right', $position ) ){
						$this->content['post_entry_top_right'][] = $content;
					}
					if ( in_array( 'post_entry_top_left', $position ) ){
						$this->content['post_entry_top_left'][] = $content;
					}
					if ( in_array( 'top_posts_sequence', $position ) ){
						$this->content['top_posts_sequence'][] = $content;
						add_action('loop_start', array( $this, 'display_content_top_posts_sequence'));
					}
					if ( in_array( 'bottom_posts_sequence', $position ) ){
						$this->content['bottom_posts_sequence'][] = $content;
						add_action('loop_end', array( $this, 'display_content_bottom_posts_sequence'));
					}
					if ( in_array( 'footer', $position )){
						$this->content['footer'][] = $content;
						add_action('wp_footer', array( $this, 'display_content_footer'), 20 );
					}
				}
			}
			add_filter( 'the_content', array( &$this, 'container_of_content' ), 110);
		}

		/**
		 * Hooks the display content header
		 *
		 * @return void
		 * @access public
		 */
		function display_content_header(){
			if ( NULL != $this->content['header']){
				foreach ($this->content['header'] as $content)
					if( self::check_show( $content->shown_in ) AND !self::check_show( $content->not_shown_in ) ){
						if ( '1' == $content->content_is_php ){
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
						} else {
							$value = do_shortcode( $content->content );
						}
						echo $value;
					}
			}
		}

		function container_of_content($post_content){
			self::display_content_after_post_entry($post_content);
			self::display_content_before_post_entry($post_content);
			self::display_content_post_entry_top_right($post_content);
			self::display_content_post_entry_top_left($post_content);

			return $post_content;
		}

		/**
		 * Hooks the display content after post entry
		 *
		 * @param string $post_content content post/page
		 * @return void
		 * @access public
		 */
		function display_content_after_post_entry(&$post_content){
			if (  isset( $this->content['after_post_entry']) ) {
				foreach ($this->content['after_post_entry'] as $content){
					if( self::check_show( $content->shown_in ) AND !self::check_show( $content->not_shown_in ) ){
						if ( '1' == $content->content_is_php ) {
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							$post_content = $post_content . $value;
						} else {
							$post_content = $post_content . do_shortcode( $content->content );
						}
					} else {
						$post_content = $post_content ;
					}
				}
			}
		}

		/**
		 * Hooks the display content before post entry
		 *
		 * @param string $post_content content post/page
		 * @return void
		 * @access public
		 */
		function display_content_before_post_entry(&$post_content){
			if ( isset( $this->content['before_post_entry']) ) {
				foreach ($this->content['before_post_entry'] as $content){
					if( self::check_show( $content->shown_in )  AND !self::check_show( $content->not_shown_in ) ) {
						if ( '1' == $content->content_is_php ) {
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							$post_content =  $value . $post_content;
						} else {
							$post_content = do_shortcode( $content->content ) . $post_content;
						}
					} else {
						$post_content = $post_content ;
					}
				}
			}
		}

		/**
		 * Hooks the display content post entry top right
		 *
		 * @param string $post_content content post/page
		 * @return void
		 * @access public
		 */
		function display_content_post_entry_top_right(&$post_content){
			if ( isset( $this->content['post_entry_top_right'] ) ) {
				foreach ($this->content['post_entry_top_right'] as $content){
					if( self::check_show( $content->shown_in )  AND !self::check_show( $content->not_shown_in ) ) {
						$top_right_width = '';

						if ($content->top_right_width != '') {
							$top_right_width = $content->top_right_width . 'px';
						}
						else {
							$top_right_width = 'auto';
						}

						if ( '1' == $content->content_is_php ) {
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							$post_content = '<div class="alignright" style="margin-left: 1.5em; margin-bottom: 2em;width:' . $top_right_width . ';">' . $value . '</div>' . $post_content;
						}
						else {
							$value = do_shortcode( $content->content );
							$post_content = '<div class="alignright" style="margin-left: 1.5em; margin-bottom: 2em;width:' . $top_right_width . ';">' . $value . '</div>' . $post_content;
						}
					}
					else {
						$post_content = $post_content ;
					}
				}
			}

		}

		/**
		 * Hooks the display content post entry top left
		 *
		 * @param string $post_content content post/page
		 * @return void
		 * @access public
		 */
		function display_content_post_entry_top_left(&$post_content){
			if ( isset( $this->content['post_entry_top_left']) ) {
				foreach ($this->content['post_entry_top_left'] as $content){
					if( self::check_show( $content->shown_in )  AND !self::check_show( $content->not_shown_in ) ){
						$top_left_width = '';

						if ($content->top_left_width != '') {
							$top_left_width = $content->top_left_width . 'px';
						}
						else {
							$top_left_width = 'auto';
						}

						if ( '1' == $content->content_is_php ){
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							$post_content = '<div class="alignleft" style="margin-right: 2em; margin-bottom: 2em;width:' . $top_left_width . ';">' . $value . '</div>' . $post_content ;
						} else {
							$value = do_shortcode( $content->content );
							$post_content = '<div class="alignleft" style="margin-right: 2em; margin-bottom: 2em;width:' . $top_left_width . ';">' . $value . '</div>' . $post_content;
						}
					} else {
						$post_content = $post_content ;
					}
				}
			}
		}

		/**
		 * Hooks the display content top posts sequence
		 *
		 * @param string $wp_query content query
		 * @return void
		 * @access public
		 */
		function display_content_top_posts_sequence( &$wp_query ){
			global $wp_the_query;
			if ( NULL != $this->content['top_posts_sequence'] and ( $wp_query === $wp_the_query ) and !is_admin() and !is_feed() and !is_robots() and !is_trackback() ){
				foreach ($this->content['top_posts_sequence'] as $content)
					if( self::check_show( $content->shown_in ) AND !self::check_show( $content->not_shown_in ) )
						if ( '1' == $content->content_is_php ){
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							echo $value . '<br />';
						} else {
							$value =  do_shortcode( $content->content  );
							echo $value . '<br />';
						}
			}
		}

		/**
		 * Hooks the display content bottom posts sequence
		 *
		 * @param string $wp_query content query
		 * @return void
		 * @access public
		 */
		function display_content_bottom_posts_sequence( &$wp_query ){
			global $wp_the_query;
			if ( NULL != $this->content['bottom_posts_sequence'] and ( $wp_query === $wp_the_query ) and !is_admin() and !is_feed() and !is_robots() and !is_trackback() ){
				foreach ($this->content['bottom_posts_sequence'] as $content)
					if( self::check_show( $content->shown_in ) AND !self::check_show( $content->not_shown_in ) )
						if ( '1' == $content->content_is_php ){
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							echo $value . '<br /> <br />';
						} else {
							$value =  do_shortcode( $content->content  );
							echo $value . '<br /> <br />';
						}
			}
		}

		/**
		 * Hooks the display content footer
		 *
		 * @return void
		 * @access public
		 */
		function display_content_footer(){
			if ( NULL != $this->content['footer']){
				foreach ($this->content['footer'] as $content)
					if( self::check_show( $content->shown_in ) AND !self::check_show( $content->not_shown_in ) )
						if ( '1' == $content->content_is_php ){
							ob_start();
							eval($content->content);
							$eval_result = ob_get_contents();
							ob_end_clean();
							$value =  do_shortcode( $eval_result );
							echo $value;
						} else {
							$value =  do_shortcode( $content->content  );
							echo $value;
						}
			}
		}

		/**
		 * Hooks the add of the main menu
		 *
		 * @return void
		 * @access public
		 */
		function admin_menu() {
			add_menu_page(__( 'Super Content Inserter', self::$i18n_prefix ), __( 'Super Content Inserter', self::$i18n_prefix ), 'manage_options', basename(__FILE__), array(&$this, 'handle_list_content'),self::$plugin_url . '/templates/images/content_block.png');
			$page_add_content  = add_submenu_page( basename(__FILE__), __( 'Content Block List', self::$i18n_prefix ), __('Content List', self::$i18n_prefix ), 'manage_options', basename(__FILE__) , array(&$this, 'handle_list_content'));
			$page_list_content = add_submenu_page( basename(__FILE__), __( 'Add/Edit Content Block', self::$i18n_prefix ), __('Add/Edit Content', self::$i18n_prefix), 'manage_options',  'super-content-inserter-add-edit.php', array(&$this, 'handle_add_content'));
			$page_help		   = add_submenu_page( basename(__FILE__), __( 'Help', self::$i18n_prefix ), __('Help', self::$i18n_prefix), 'manage_options',  'super-content-inserter-help.php', array(&$this, 'handle_help'));

			add_action( "admin_print_scripts-$page_add_content", array(&$this, 'handle_admin_scripts') );
			add_action( "admin_print_scripts-$page_list_content", array(&$this, 'handle_admin_scripts') );
			add_action( "admin_print_scripts-$page_help", array(&$this, 'handle_admin_scripts') );
		}

		/**
		 * Handles the js variables for handle_admin_scripts
		 *
		 */
		function handle_admin_scripts() {
			wp_localize_script(
				'jquery'
				, 'SCI'
				, array(
					'plugin_url' => self::$plugin_url
				)
			);
		}

		/**
		 * Handles the main menu for handle_list_content
		 *
		 * Used for: list Contents
		 *
		 * @return void
		 * @access public
		 */
		function handle_list_content() {
			// Load needed styles.
			wp_enqueue_style('thickbox');

			wp_enqueue_style('twitter-bootstrap', self::$plugin_url . 'templates/js/lib/bootstrap/css/bootstrap.css');
			wp_enqueue_style('sci-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/css/DT_bootstrap.css');

			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			// Load needed script libraries.
			wp_enqueue_script('thickbox');
			wp_enqueue_script('jquery-effects-highlight');
			wp_enqueue_script('jquery-scrollTo', self::$plugin_url . 'templates/js/lib/jquery.scrollTo.js');
			wp_enqueue_script('jquery-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/js/jquery.dataTables.min.js');
			wp_enqueue_script('jquery-dataTables-bootstrap', self::$plugin_url . 'templates/js/lib/dataTables/js/DT_bootstrap.js');
			wp_enqueue_script('sci-add-edit-content', self::$plugin_url . 'templates/js/list.js');

			include(self::$plugin_dir . '/includes/admin/handle_list_content.php');
		}

		/**
		 * Handles the main menu for handle_add_content
		 *
		 * Used for: add/edit a Content
		 *
		 * @return void
		 * @access public
		 */
		function handle_add_content() {
			// Select the Text editor by default.
			add_filter( 'wp_default_editor', create_function('', 'return "html";') );

			// Load needed styles.
			wp_enqueue_style('jquery-chosen', self::$plugin_url . 'templates/js/lib/chosen/chosen.css');
			wp_enqueue_style('jquery-iCheckbox', self::$plugin_url . 'templates/js/lib/iCheckbox/css/iCheckbox.css');
			wp_enqueue_style('sci-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/css/jquery.dataTables.css');
			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			// Load needed script libraries.
			wp_enqueue_script('jquery-effects-highlight');
			wp_enqueue_script('jquery-scrollTo', self::$plugin_url . 'templates/js/lib/jquery.scrollTo.js');
			wp_enqueue_script('jquery-form', self::$plugin_url . 'templates/js/lib/jquery.form.js');
			wp_enqueue_script('jquery-chosen', self::$plugin_url . 'templates/js/lib/chosen/chosen.js');
			wp_enqueue_script('jquery-iCheckbox', self::$plugin_url . 'templates/js/lib/iCheckbox/jquery.iCheckbox.js');
			wp_enqueue_script('jquery-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/js/jquery.dataTables.min.js');
			wp_enqueue_script('sci-add-edit-content', self::$plugin_url . 'templates/js/add-edit.js');

			include(self::$plugin_dir . '/includes/admin/handle_add_content.php');
		}

		/**
		 * Handles the main menu for handle_help.
		 *
		 * Used for: Help.
		 *
		 * @return void
		 * @access public
		 */
		function handle_help() {
			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			include(self::$plugin_dir . '/includes/admin/handle_help.php');
		}

		/**
		 * To verify if content will show
		 *
		 * @param string $shown show in not in content
		 * @return bool true or false
		 * @access public
		 */
		function check_show($shown){
			// Explode by convert string intro array with ';' character separator.
			$shown_arr =  explode(';', $shown );
			$shown_split = array(); // Output array [home],[pages,8,0,9],[posts,all_posts] ...
			// In each $shown_arr array there are list to the other content, with '::' character separator.
			foreach ( $shown_arr as $shown_arr_item ){
				$shown_arr_multi = explode('::', $shown_arr_item );
				$shown_split[ $shown_arr_multi[0] ] = $shown_arr_multi;
			}

			global $post;
			if ( in_array( 'all', $shown_arr) ){
				return true;
			} elseif (in_array( 'home', $shown_arr) AND is_front_page()) {
				return true; // This is home
			} elseif ( isset( $shown_split['pages'] ) AND 'page' == $post->post_type AND ( in_array( 'all_pages', $shown_split['pages'] ) OR in_array( $post->ID, $shown_split['pages'] ) ) ){
				return true; // This is a page
			} elseif ( isset( $shown_split['posts'] ) AND 'post' == $post->post_type  AND ( in_array( 'all_posts', $shown_split['posts'] ) OR in_array( $post->ID, $shown_split['posts'] ) ) ){
				return true; // This is a post
			} elseif ( isset( $shown_split['categories'] ) AND 'post' == $post->post_type AND !is_archive() AND !is_404() AND (  in_array( 'all_categories', $shown_split['categories'] ) OR  NULL != array_intersect(  wp_get_post_categories($post->ID) , $shown_split['categories'] ) ) ){
				return true; // This is a category
			} elseif ( isset( $shown_split['tags'] ) AND 'post' == $post->post_type AND !is_archive() AND !is_404() AND (  in_array( 'all_tags', $shown_split['tags'] ) OR NULL != array_intersect(  wp_get_post_tags($post->ID, array( 'fields' => 'ids' ) ) , $shown_split['tags'] ) ) ){
				return true; // This is a tags
			} elseif ( in_array( '404_page', $shown_arr) AND is_404()) {
				return true; // This is a page 404
			} elseif ( isset( $shown_split['category_page'] ) AND is_category() AND ( in_array( 'all_category_page', $shown_split['category_page'] ) OR in_category($shown_split['category_page']) ) ){
				return true; // This is a post
			} elseif ( isset( $shown_split['tags_page'] ) AND is_tag() AND ( in_array( 'all_tags_page', $shown_split['tags_page'] ) OR self::in_tag( $shown_split['tags_page'] ) ) ){
				return true; // This is a post
			} elseif ( in_array( 'archives', $shown_arr) AND is_archive() AND !is_category() AND !is_tag() AND !is_page() ) {
				return true; // This is a page 404
			}

			return false;
		}

		/**
		 * Function search in tag
		 *
		 * @param array $tags_id list of tags id
		 * @return bool true or false
		 * @access public
		 */
		function in_tag($tags_id){
			foreach ( $tags_id as $tag )
				if ( SuperContentInserter_Validator::parse_int($tag) AND true == is_tag( get_tag($tag)->slug ) )
					return true;
			return false;
		}

		function install() {
			// Creating tables
			include(self::$plugin_dir . '/db/tables.php');
		}

		/**
		 * Execute code in deactivation
		 *
		 * @return 	void
		 * @access 	public
		 */
		function uninstall() {
			// TODO: DROP DB
		}
	}
}

$exit_msg = __('This plugin require WordPress 3.1 or newer',SuperContentInserter::$i18n_prefix).'. <a href="http://codex.wordpress.org/Upgrading_WordPress">'.__('Please update',SuperContentInserter::$i18n_prefix).'</a>';
if (version_compare($wp_version, "3.1", "<")) {
	exit($exit_msg);
}

// create new instance of the class
$SuperContentInserter = new SuperContentInserter();
if (isset($SuperContentInserter)) {
	// register the activation-deactivation function by passing the reference to our instance
	register_activation_hook(__FILE__, array(&$SuperContentInserter, 'install'));
	register_deactivation_hook(__FILE__, array(&$SuperContentInserter, 'uninstall'));
}